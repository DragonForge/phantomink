package com.zeitheron.phantomink;

import java.util.HashMap;
import java.util.Map;

import com.zeitheron.hammercore.internal.SimpleRegistration;

import net.minecraft.client.resources.I18n;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.client.event.RenderPlayerEvent;
import net.minecraftforge.event.entity.player.ItemTooltipEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@EventBusSubscriber
@Mod(modid = "phantomink", name = "Phantom Ink", version = "@VERSION@", dependencies = "required-after:hammercore", certificateFingerprint = "4d7b29cd19124e986da685107d16ce4b49bc0a97", updateJSON = "https://raw.githubusercontent.com/Zeitheron/updates/master/phantomink.json")
public class PhantomInk
{
	public static final Item PHANTOM_INK_BOTTLE = new Item().setTranslationKey("phantom_ink_bottle");
	
	public static final String NBT_TAG_PHANTOM = "PhantomInk_Phantom";
	
	public static void setPhantom(ItemStack stack, boolean phantom)
	{
		if(stack.isEmpty())
			return;
		if(!stack.hasTagCompound() && phantom)
			stack.setTagCompound(new NBTTagCompound());
		if(phantom)
			stack.getTagCompound().setBoolean(NBT_TAG_PHANTOM, phantom);
		else
		{
			stack.getTagCompound().removeTag(NBT_TAG_PHANTOM);
			if(stack.getTagCompound().isEmpty())
				stack.setTagCompound(null);
		}
	}
	
	public static boolean isPhantom(ItemStack stack)
	{
		return !stack.isEmpty() && stack.hasTagCompound() && stack.getTagCompound().getBoolean(NBT_TAG_PHANTOM);
	}
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent e)
	{
		SimpleRegistration.registerFieldItemsFrom(PhantomInk.class, "phantomink", CreativeTabs.MISC);
	}
	
	/// Client events
	
	@SideOnly(Side.CLIENT)
	@SubscribeEvent(priority = EventPriority.HIGHEST)
	public static void tooltipEvent(ItemTooltipEvent e)
	{
		if(e.getItemStack().getItem() instanceof ItemArmor && isPhantom(e.getItemStack()))
			e.getToolTip().add(I18n.format("text.phantomink:phantom"));
	}
	
	private static Map<EntityEquipmentSlot, ItemStack> armor = new HashMap<>();
	
	@SideOnly(Side.CLIENT)
	@SubscribeEvent(priority = EventPriority.LOWEST)
	public static void preRenderPlayer(RenderPlayerEvent.Pre e)
	{
		EntityPlayer player = e.getEntityPlayer();
		for(EntityEquipmentSlot s : EntityEquipmentSlot.values())
			if(s.ordinal() > 1)
			{
				ItemStack stack = player.getItemStackFromSlot(s);
				if(isPhantom(stack))
				{
					player.inventory.armorInventory.set(s.getIndex(), ItemStack.EMPTY);
					armor.put(s, stack);
				}
			}
	}
	
	@SideOnly(Side.CLIENT)
	@SubscribeEvent(priority = EventPriority.HIGHEST)
	public static void postRenderPlayer(RenderPlayerEvent.Post e)
	{
		EntityPlayer player = e.getEntityPlayer();
		for(EntityEquipmentSlot s : EntityEquipmentSlot.values())
			if(s.ordinal() > 1 && armor.containsKey(s))
				player.inventory.armorInventory.set(s.getIndex(), armor.remove(s));
	}
}