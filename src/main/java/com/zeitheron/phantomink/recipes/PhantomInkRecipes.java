package com.zeitheron.phantomink.recipes;

import com.zeitheron.hammercore.utils.recipes.helper.RecipeRegistry;
import com.zeitheron.hammercore.utils.recipes.helper.RegisterRecipes;
import com.zeitheron.phantomink.PhantomInk;

import net.minecraft.init.Items;
import net.minecraft.init.PotionTypes;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionHelper;
import net.minecraft.potion.PotionUtils;
import net.minecraft.util.ResourceLocation;

@RegisterRecipes(modid = "phantomink")
public class PhantomInkRecipes extends RecipeRegistry
{
	@Override
	public void crafting()
	{
		shapeless(PhantomInk.PHANTOM_INK_BOTTLE, PotionUtils.addPotionToItemStack(new ItemStack(Items.POTIONITEM), PotionTypes.WATER), new ItemStack(Items.DYE, 1, 0), new ItemStack(Items.GOLD_NUGGET));
		recipe(new RecipeApplyPhantomInk().setRegistryName(new ResourceLocation(getMod(), "phantom_ink.apply")));
		recipe(new RecipeRemovePhantomInk().setRegistryName(new ResourceLocation(getMod(), "phantom_ink.remove")));
	}
}